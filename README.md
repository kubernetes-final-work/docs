# Skillbox. Final work of the Kubernetes module

## Project Topic: "Kubernetes Cluster for an Online Platform"

### Repository Directory Structure

The group https://gitlab.com/kubernetes-final-work/ contains several projects:
1. #### [terraform](https://gitlab.com/kubernetes-final-work/terraform)
The `terraform` repository includes code for deploying virtual infrastructure on AWS using Terraform.

The infrastructure comprises:
   - A VPC containing public and private subnets across multiple AZs, a nat gateway.
   - One virtual server in the public subnet (bastion-server).
   - Five virtual servers for deploying the Kubernetes cluster in a private subnet.
   - A Network Load Balancer to access cluster services from the outside.

2. #### [ansible](https://gitlab.com/kubernetes-final-work/ansible)
The `ansible` repository contains ansible code for installing necessary software in the cluster using the following roles:
   - **traefik** - deploys Traefik on the bastion server.
   - **app** - deploys a microservice application using ready-made manifests.
   - **kube-gitlab-runner** - for deploying gitlab-runner in the cluster using Helm Charts.
   - **kube-grafana**, **kube-loki**, **kube-prometheus** - deploying services for monitoring with Helm.

3. #### [service](https://gitlab.com/kubernetes-final-work/service)
The `service` repository directly contains the microservice (loadgenerator) implemented in Python and GitLab CI pipelines for testing, building, and deploying it.

4. #### [kubespray](https://gitlab.com/kubernetes-final-work/kubespray)
The `kubespray` repository holds ansible playbooks for deploying all components of the Kubernetes cluster and preparing it for operation. The inventory file **inventory.ini** and main settings for deploying the cluster are located in the directory **kubespray/inventory/mycluster**

5. #### [Docs](https://gitlab.com/kubernetes-final-work/Docs)
The `docs` repository contains:
   - Required screenshots and configuration files (screenshots directory) in accordance with the technical assignment.
   - The script **create_infra.sh** for initiating the cluster deployment process.

## Cluster Deployment and Configuration

To deploy the cluster, the following steps must be taken:

   - Clone all the repositories described above into a previously created directory on a PC or virtual machine.
   - Navigate to the terraform directory of the cloned repository and run the script `./create_infra.sh` (it must first be copied to the root of the previously created directory containing all repositories), which creates and configures the entire infrastructure using terraform and ansible code.
   - After successful cluster creation, you can proceed to deploy the microservice (`loadgenerator`).


