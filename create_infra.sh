#!/bin/bash

cd terraform
terraform init && terraform plan && terraform apply -auto-approve || exit 1

cd ..

cd ansible
ansible-playbook -b --vault-password-file=psw.txt traefik-install.yaml -vv  || exit 1

cd ..
cd kubespray
ansible-playbook -i inventory/mycluster/inventory.ini  --become --become-user=root cluster.yml -vv

if [ $? != 0 ]; then exit 1; fi

cd ..
cd ansible
ansible-playbook --vault-password-file=psw.txt runner-install.yaml -vv  || exit
